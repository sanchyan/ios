//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/ToolsDefines.h>
#import <Tools/PTSelectableBarButtonItem.h>

NS_ASSUME_NONNULL_BEGIN

PT_EXPORT
PT_OBJC_RUNTIME_NAME(ToolBarButtonItem)
@interface PTToolBarButtonItem : PTSelectableBarButtonItem <NSCopying>

- (instancetype)initWithToolClass:(Class)toolClass target:(nullable id)target action:(nullable SEL)action;

@property (nonatomic, strong, nullable) Class toolClass;

@property (nonatomic, copy, nullable) NSString *identifier;

/**
 * Whether this button item shows the current, default annotation style for the `toolClass`. For tool
 * classes that do not create annotations or allow editing the style of annotations, this property
 * has no effect.
 *
 * The default value of this property is `YES`.
 */
@property (nonatomic, assign) BOOL showsAnnotationStyle;

@end

NS_ASSUME_NONNULL_END
