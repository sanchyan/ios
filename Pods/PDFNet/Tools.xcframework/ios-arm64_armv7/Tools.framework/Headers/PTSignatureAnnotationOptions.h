//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTAnnotationOptions.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString* _Nonnull const PTStoreNewSignatureKey;

/**
 * An object that contains options for signature annotations.
 */
PT_OBJC_RUNTIME_NAME(SignatureAnnotationOptions)
@interface PTSignatureAnnotationOptions : PTAnnotationOptions

/**
 * Whether the annotation's appearance (strokes) can be edited. The default value is `NO`.
 */
@property (nonatomic, assign) BOOL canEditAppearance;

/**
 * If true, signature fields will be signed by placing a stamp on top of them rather than
 * changing the field's appearance. Default is `NO`.
 */
@property (nonatomic, assign) BOOL signSignatureFieldsWithStamps;

/**
 * If true, signature fields will be considered signed if there is an ink
 * or stamp annotation overlapping it. Default is `YES`.
 */
@property (nonatomic, assign, getter=isWidgetSigningWithOverlappingAnnotationEnabled) BOOL widgetSigningWithOverlappingAnnotationEnabled;

/**
 * If true, newly created signature will be saved to the signature list.
 * Default is `YES`.
 */
@property (nonatomic, assign) BOOL storeNewSignature;

/**
 * If true, `storeNewSignature` will be read from NSUserDefaults.
 * Default is `YES`.
 */
@property (nonatomic, assign) BOOL persistStoreSignatureSetting;

@end

NS_ASSUME_NONNULL_END
