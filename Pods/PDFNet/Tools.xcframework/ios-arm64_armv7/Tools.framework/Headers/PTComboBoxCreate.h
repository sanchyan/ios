//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2021 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/Tools.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates combo box form annotations.
 */
PT_OBJC_RUNTIME_NAME(ComboBoxCreate)
@interface PTComboBoxCreate : PTRectangleCreate

@end

NS_ASSUME_NONNULL_END
