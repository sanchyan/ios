//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTFreeTextCreate.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates free text callout (`PTExtendedAnnotTypeCallout`) annotations.
 */
PT_OBJC_RUNTIME_NAME(CalloutCreate)
@interface PTCalloutCreate : PTFreeTextCreate

@end

NS_ASSUME_NONNULL_END
