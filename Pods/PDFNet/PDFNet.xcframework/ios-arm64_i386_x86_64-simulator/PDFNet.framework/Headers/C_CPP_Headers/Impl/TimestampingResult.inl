#ifdef SWIG
inline TimestampingResult::TimestampingResult()
	: m_impl(0)
	, m_owner(false)
{}
#endif


inline TimestampingResult::~TimestampingResult()
{
	Destroy();
}

inline TimestampingResult::TimestampingResult(const TimestampingResult& other)
	: m_impl(other.m_impl)
	, m_owner(false)
{
	if(other.m_owner) {
		other.m_owner = false;
		m_owner = true;
	}
}

inline void TimestampingResult::Destroy()
{
	if(m_owner)
	{
		DREX(m_impl, TRN_TimestampingResultDestroy(m_impl));
		m_owner = false;
	}
}

inline TimestampingResult& pdftron::PDF::TimestampingResult::operator= (const TimestampingResult& other)
{
	Destroy();

	m_impl = other.m_impl;

	if(other.m_owner) {
		other.m_owner = false;
		m_owner = true;
	}

	return *this;
}


inline TimestampingResult::TimestampingResult(TRN_TimestampingResult impl)
	: m_impl(impl)
	, m_owner(true)
{}

inline bool TimestampingResult::GetStatus() const
{
	TRN_Bool result = 0;
	REX(TRN_TimestampingResultGetStatus((TRN_TimestampingResult)m_impl, &result));
	return result != 0;
}

inline UString TimestampingResult::GetString() const
{
	UString result;
	REX(TRN_TimestampingResultGetString((TRN_TimestampingResult)m_impl, (TRN_UString*)&result.mp_impl));
	return result;
}

inline bool TimestampingResult::HasResponseVerificationResult() const
{
	TRN_Bool result = 0;
	REX(TRN_TimestampingResultHasResponseVerificationResult((TRN_TimestampingResult)m_impl, &result));
	return result != 0;
}

inline EmbeddedTimestampVerificationResult TimestampingResult::GetResponseVerificationResult() const
{
	TRN_EmbeddedTimestampVerificationResult result;
	REX(TRN_TimestampingResultGetResponseVerificationResult((TRN_TimestampingResult)m_impl, (TRN_EmbeddedTimestampVerificationResult*) &result));
	return EmbeddedTimestampVerificationResult(result);
}

inline std::vector<pdftron::UChar> TimestampingResult::GetData() const
{
	TRN_Vector cvector;
	REX(TRN_TimestampingResultGetData((TRN_TimestampingResult) m_impl, &cvector));
	void* arr;
	UInt32 size;
	REX(TRN_VectorGetData(cvector, &arr));
	REX(TRN_VectorGetSize(cvector, &size));
	std::vector<UChar> result(size);
	memcpy(&result[0], arr, size);
	TRN_VectorDestroy(cvector);
	return result;
}
