//
//  ViewController.m
//  pdftron
//
//  Created by Sanchyan Chakraborty on 03/01/22.
//

#import "ViewController.h"
// Imported PDF Tron Header
#import <PDFNet/PDFNet.h>
#import <Tools/Tools.h>

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"ViewController.m viewDidLoad >>>> ");
  
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSLog(@"ViewController.m viewDidAppear >>>> ");
    [self generateNewSDKPdf]; //Calling to Generate PDF
    
}

-(void)generateNewSDKPdf
{
    
    @try{
    
    NSString   *fileName = @"output.pdf";
        
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
      //  NSString   *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
        
        NSURL *documentsDirectoryURL = [NSURL fileURLWithPath:documentsDirectory];
        NSString   *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
        NSURL *outputFileURL = [documentsDirectoryURL URLByAppendingPathComponent:fileName];

        NSLog(@"This path should be save pdf after generated text image and check box >>> %@",pdfFileName);
        NSURL *documentURL = [NSBundle.mainBundle URLForResource:@"template" withExtension:@"pdf"];
        
        NSLog(@"This url to grab the pdf template %@",documentURL.absoluteString);
        
        // If I comment the below PTPDFDoc, then blank PDF Template opens
        
       PTPDFDoc *doc = [[PTPDFDoc alloc] initWithFilepath: documentURL.path];

        // First modify the field
        @try{
            PTFieldIterator * itr;
            for(itr = [doc GetFieldIterator]; [itr HasNext]; [itr Next])
            {
                PTField *field = [itr Current];
                
                if ([[field GetName]  isEqual: @"text"])
                {
                    [field SetValueWithString:@"Programatically Text Added"];
                    [field RefreshAppearance];
                }
                else  if ([[field GetName]  isEqual: @"text_2"])
                {
                    [field SetValueWithString:@"Text 2 Programatically Text Added"];
                    [field RefreshAppearance];
                }
                else  if ([[field GetName]  isEqual: @"check"])
                {
                    [field SetValueWithBool:TRUE];
                    [field RefreshAppearance];
                }
                else  if ([[field GetName]  isEqual: @"check_2"])
                {
                    [field SetValueWithBool:TRUE];
                    [field RefreshAppearance];
                }


                // Alternatively: check the field type
                int type = [field GetType];
                switch(type)
                {
                    case e_ptbutton:
                        NSLog(@"Field type: Button");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        break;
                    case e_ptcheck:
                        NSLog(@"Field type: Check");
                        NSLog(@"Field name: %@", [field GetName]);
                       
                        NSLog(@"------------------------------");
                        break;
                    case e_ptradio:
                        NSLog(@"Field type: Radio");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        break;
                    case e_pttext:
                        NSLog(@"Field type: Text");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        // Do something with text fields
                        break;
                    case e_ptchoice:
                        NSLog(@"Field type: Choice");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        break;
                    case e_ptsignature:
                        NSLog(@"Field type: Signature");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        break;
                    case e_ptf_null:
                        NSLog(@"Field type: Null");
                        NSLog(@"Field name: %@", [field GetName]);
                        NSLog(@"------------------------------");
                        break;
                    default: NSLog(@"Field type: ------------------------------");
                }
            }
        }
        @catch (NSException *exception) {
            
        }

        // Then save the document (in this example I'm just saving it to the temporary directory)
        NSString *tmpDirectory = NSTemporaryDirectory();
        NSString *tmpFile = [tmpDirectory stringByAppendingPathComponent:@"temp.pdf"];
       // [doc SaveToFile:tmpFile flags:e_ptremove_unused];
        [doc FlattenAnnotations:YES];
        //[doc SaveToFile:pdfFileName flags:e_ptremove_unused];
        [doc SaveToFile:outputFileURL.path flags:e_ptremove_unused];
        NSLog(@"Checking file path >>> %@",pdfFileName);
        
        NSLog(@"Checking outputFileURL.path >>> %@",outputFileURL.path);
        NSURL *tmpURL = [NSURL fileURLWithPath:pdfFileName];
        
        NSLog(@"Checking tmpURL path >>> %@",tmpURL);
        
        PTDocumentController *documentController = [[PTDocumentController alloc] init];
           // The PTDocumentController must be in a navigation controller before a document can be opened
        documentController.toolGroupIndicatorView.hidden = YES;
        documentController.searchButtonHidden = YES;
        documentController.moreItemsButtonHidden = YES;
        
       // [documentController openDocumentWithPDFDoc:doc];
       // [documentController openDocumentWithURL:tmpURL];
        
        [documentController openDocumentWithURL:outputFileURL];
        
        
        
        NSMutableArray* bottomRightItems = [documentController.thumbnailSliderController.trailingToolbarItems mutableCopy];
        [bottomRightItems removeObject:documentController.navigationListsButtonItem];
       // [bottomRightItems addObject:documentViewController.searchButtonItem];
        
        NSMutableArray* rightItems = [documentController.navigationItem.rightBarButtonItems mutableCopy];
        [rightItems removeObject:documentController.searchButtonItem];
       // [rightItems addObject:documentController.navigationListsButtonItem];
        
        NSMutableArray* leftItems = [documentController.navigationItem.leftBarButtonItems mutableCopy];
        
        UIBarButtonItem *btndone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:nil];
        UIBarButtonItem *btnopenin = [[UIBarButtonItem alloc] initWithTitle:@"Open In" style:UIBarButtonItemStylePlain target:self action:nil];

        documentController.navigationItem.rightBarButtonItems = [rightItems copy];
        documentController.navigationItem.leftBarButtonItems = [leftItems copy];
        documentController.thumbnailSliderController.trailingToolbarItems = [bottomRightItems copy];
   
        documentController.navigationItem.leftBarButtonItems = [documentController.navigationItem.leftBarButtonItems arrayByAddingObject:btndone];
        documentController.navigationItem.rightBarButtonItems = [documentController.navigationItem.rightBarButtonItems arrayByAddingObject:btnopenin];
        
           UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:documentController];
           navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
           navigationController.navigationBar.translucent = NO;
           navigationController.toolbar.translucent = NO;
        
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
        appearance.backgroundColor = UIColor.yellowColor;
        navigationController.navigationBar.standardAppearance = appearance;
        navigationController.navigationBar.scrollEdgeAppearance = appearance;
        
      
      
           // Open a file from URL.
         
           // Show navigation (and document) controller.
      
           [self presentViewController:navigationController animated:YES completion:nil];
      
   
      
      
     }
    @catch (NSException *exception) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Exception Occured generate  pdf method" message:exception.description preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                //button click event
                            }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [alert addAction:ok];
       
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

@end
