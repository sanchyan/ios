//
//  SceneDelegate.h
//  pdftron
//
//  Created by Sanchyan Chakraborty on 03/01/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

